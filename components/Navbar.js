import {useState} from 'react';
import Link from 'next/link';
import {useRouter} from 'next/router';

export default function Navbar() {
  // nav menu toggle 
  const [navbarOpen, setNavbarOpen] = useState(false);
  const navToggle = ()=>{
    setNavbarOpen(!navbarOpen);
  }
  // console.log(navbarOpen)
  // dropdown toggle 
  const[navOne, setNavOne] = useState(true);
  const[navTwo, setNavTwo] = useState(true);
  const closeNavDropdown =()=>{
    setNavOne(true);
    setNavTwo(true);
  }
  // active nav class 
  const router = useRouter();
  // console.log(router)
  let isActive = (route)=>{
    return route == router.pathname ?  "activeLink" : "";
  }
  return (
    <>
  
      <div className="navbar_main bg-white z-50 sticky top-0" style={{zIndex:1001}}>
        <div className=" mx-auto">
        {/* <div className=" mx-auto px-4 sm:px-6"> */}
        {/* <div className="max-w-7xl mx-auto px-4 sm:px-6"> */}
          <div className="flex  px-4 md:px-16 justify-between items-center border-b-2 border-gray-100 py-6 md:justify-star md:space-x-10 xl:space-x-52">
            <div className="lg:w-auto lg:flex">
              <Link href="/" >
                <a className="flex">
                  <img className="h-8 w-auto sm:h-10" src="/images/farmgate_logo.svg" alt="farmgate logo"/>
                </a>
              </Link>
            </div>
            <div className="-mr-2 -my-2 md:hidden">
              <button type="button" className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out" onClick={navToggle}>
                {/* <!-- Heroicon name: menu --> */}
                <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
              </button>
            </div>
            {/* <nav className="hidden md:flex lg:space-x-10"> */}
            <nav className="hidden md:flex md:space-x-6 lg:space-x-10">
              {/* button closes any navbar dropdown  */}
              <button onClick={closeNavDropdown} className={"cursor-default inset-0 w-full h-full fixed bg-none focus:border-none focus:bg-none focus:outline-none;" + ((navOne == false || navTwo == false) ? " flex" : " hidden")}></button>
              <Link href="/#region_section" >
                <a className="text-base leading-4 font-normal focus:font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition ease-in-out duration-150">
                Regions
                </a>
              </Link>
              <Link href="/#about_us_section" >
                <a className="text-base leading-4 font-normal focus:font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition ease-in-out duration-150">
                  About us
                </a>
              </Link>
              <Link href="/#" >
                <a className="text-base leading-4 font-normal focus:font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition ease-in-out duration-150">
                  Contact us
                </a>
              </Link>
        
            </nav>
            <div className="auth_container hidden md:flex items-center justify-end space-x-8 md:flex-1 lg:w-0">
              {/* <Link href="/register/options">
                <a className={`whitespace-no-wrap text-base leading-4 font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 `}>
                Sign up
                </a>
              </Link> */}
              <Link href="/login/options">
                <a className={`whitespace-no-wrap text-base leading-4 font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900  `}>
                  Log in
                </a>
              </Link>
            </div>
          </div>
        </div>

        {/* <!--
          Mobile menu, show/hide based on mobile menu state.

          Entering: "duration-200 ease-out"
            From: "opacity-0 scale-95"
            To: "opacity-100 scale-100"
          Leaving: "duration-100 ease-in"
            From: "opacity-100 scale-100"
            To: "opacity-0 scale-95"
        --> */}
  
        <div className={"absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden" + (navbarOpen ? " block menu_drop_in" : " hidden menu_drop_out")} >
          <div className="rounded-lg shadow-lg">
            <div className="rounded-lg shadow-xs bg-white divide-y-2 divide-gray-50">
              <div className="pt-5 pb-6 px-5 space-y-6">
                <div className="flex items-center justify-between">
                  <div>
                    <img className="h-8 w-auto" src="/images/farmgate_logo.svg" alt="farmgate logo"/>
                  </div>
                  <div className="-mr-2">
                    <button type="button" className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out" onClick={navToggle}>
                      {/* <!-- Heroicon name: x --> */}
                      <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                      </svg>
                    </button>
                  </div>
                </div>
                <div>
                  <nav className="grid gap-y-8 pt-4">
                  <Link href="/#region_section" >
                    <a className="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150" onClick={navToggle}>
                      {/* <svg  className="flex-shrink-0 h-6 w-6" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M4 6C4 5.46957 4.21071 4.96086 4.58579 4.58579C4.96086 4.21071 5.46957 4 6 4H8C8.53043 4 9.03914 4.21071 9.41421 4.58579C9.78929 4.96086 10 5.46957 10 6V8C10 8.53043 9.78929 9.03914 9.41421 9.41421C9.03914 9.78929 8.53043 10 8 10H6C5.46957 10 4.96086 9.78929 4.58579 9.41421C4.21071 9.03914 4 8.53043 4 8V6ZM14 6C14 5.46957 14.2107 4.96086 14.5858 4.58579C14.9609 4.21071 15.4696 4 16 4H18C18.5304 4 19.0391 4.21071 19.4142 4.58579C19.7893 4.96086 20 5.46957 20 6V8C20 8.53043 19.7893 9.03914 19.4142 9.41421C19.0391 9.78929 18.5304 10 18 10H16C15.4696 10 14.9609 9.78929 14.5858 9.41421C14.2107 9.03914 14 8.53043 14 8V6ZM4 16C4 15.4696 4.21071 14.9609 4.58579 14.5858C4.96086 14.2107 5.46957 14 6 14H8C8.53043 14 9.03914 14.2107 9.41421 14.5858C9.78929 14.9609 10 15.4696 10 16V18C10 18.5304 9.78929 19.0391 9.41421 19.4142C9.03914 19.7893 8.53043 20 8 20H6C5.46957 20 4.96086 19.7893 4.58579 19.4142C4.21071 19.0391 4 18.5304 4 18V16ZM14 16C14 15.4696 14.2107 14.9609 14.5858 14.5858C14.9609 14.2107 15.4696 14 16 14H18C18.5304 14 19.0391 14.2107 19.4142 14.5858C19.7893 14.9609 20 15.4696 20 16V18C20 18.5304 19.7893 19.0391 19.4142 19.4142C19.0391 19.7893 18.5304 20 18 20H16C15.4696 20 14.9609 19.7893 14.5858 19.4142C14.2107 19.0391 14 18.5304 14 18V16Z" stroke="#63A848" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                      </svg> */}
                      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.9996 10.0002C13.6569 10.0002 14.9996 8.65751 14.9996 7.00024C14.9996 5.34298 13.6569 4.00024 11.9996 4.00024C10.3424 4.00024 8.99963 5.34298 8.99963 7.00024C8.99963 8.65751 10.3424 10.0002 11.9996 10.0002ZM11.9996 6.00026C12.5523 6.00026 12.9996 6.44754 12.9996 7.00024C12.9996 7.55295 12.5523 8.00023 11.9996 8.00023C11.4469 8.00023 10.9996 7.55295 10.9996 7.00024C10.9996 6.44754 11.4469 6.00026 11.9996 6.00026Z" fill="#63A848"/>
                        <path d="M10.6982 18.6336L11.1052 19.4475C11.4737 20.1846 12.5257 20.1845 12.8941 19.4473L13.7561 17.7223C14.6217 15.9925 15.4407 14.6276 17.3075 11.6933L17.356 11.6169C17.6304 11.1857 17.763 10.977 17.9205 10.7281C18.6216 9.6208 18.9996 8.33788 18.9996 7.0003C18.9996 2.82692 15.3738 -0.430373 11.1815 0.0467206C8.06204 0.402549 5.50079 2.89836 5.06907 6.00571C4.81074 7.86585 5.28489 9.68042 6.35935 11.1393C7.8047 13.0985 8.91765 15.0738 10.6982 18.6336ZM7.0501 6.28086C7.3561 4.07825 9.1951 2.28627 11.4079 2.03385C14.4105 1.69213 16.9996 4.01806 16.9996 7.0003C16.9996 7.95608 16.7308 8.8685 16.2307 9.65839C16.0742 9.9057 15.9422 10.1135 15.6686 10.5434L15.6201 10.6198C13.7381 13.578 12.8927 14.9848 11.9976 16.7675C10.4415 13.7036 9.35987 11.8377 7.96937 9.95267C7.2017 8.91031 6.86424 7.61895 7.0501 6.28086Z" fill="#63A848"/>
                        <path d="M18.2288 14.6738C17.6914 14.5463 17.1525 14.8787 17.025 15.416C16.8975 15.9534 17.2299 16.4924 17.7673 16.6198C20.4491 17.2557 22 18.2732 22 18.9998C22 20.3815 17.5493 21.9998 12 21.9998C6.45009 21.9998 2.00002 20.3817 2.00002 18.9998C2.00002 18.2728 3.54938 17.2556 6.22983 16.6198C6.7672 16.4923 7.0995 15.9534 6.97205 15.416C6.84459 14.8786 6.30562 14.5463 5.76825 14.6738C2.25961 15.5061 0 16.9896 0 18.9998C0 22.0375 5.39691 23.9998 12 23.9998C18.6025 23.9998 24 22.0374 24 18.9998C24 16.9896 21.739 15.5062 18.2288 14.6738Z" fill="#63A848"/>
                      </svg>

                      <div className="text-base leading-6 font-medium text-gray-900">
                      Regions
                      </div>
                    </a>
                  </Link>
                  <Link href="/#about_us_section" >
                    <a className="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150" onClick={navToggle}>
                      <svg  className="flex-shrink-0 h-6 w-6" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M4 6C4 5.46957 4.21071 4.96086 4.58579 4.58579C4.96086 4.21071 5.46957 4 6 4H8C8.53043 4 9.03914 4.21071 9.41421 4.58579C9.78929 4.96086 10 5.46957 10 6V8C10 8.53043 9.78929 9.03914 9.41421 9.41421C9.03914 9.78929 8.53043 10 8 10H6C5.46957 10 4.96086 9.78929 4.58579 9.41421C4.21071 9.03914 4 8.53043 4 8V6ZM14 6C14 5.46957 14.2107 4.96086 14.5858 4.58579C14.9609 4.21071 15.4696 4 16 4H18C18.5304 4 19.0391 4.21071 19.4142 4.58579C19.7893 4.96086 20 5.46957 20 6V8C20 8.53043 19.7893 9.03914 19.4142 9.41421C19.0391 9.78929 18.5304 10 18 10H16C15.4696 10 14.9609 9.78929 14.5858 9.41421C14.2107 9.03914 14 8.53043 14 8V6ZM4 16C4 15.4696 4.21071 14.9609 4.58579 14.5858C4.96086 14.2107 5.46957 14 6 14H8C8.53043 14 9.03914 14.2107 9.41421 14.5858C9.78929 14.9609 10 15.4696 10 16V18C10 18.5304 9.78929 19.0391 9.41421 19.4142C9.03914 19.7893 8.53043 20 8 20H6C5.46957 20 4.96086 19.7893 4.58579 19.4142C4.21071 19.0391 4 18.5304 4 18V16ZM14 16C14 15.4696 14.2107 14.9609 14.5858 14.5858C14.9609 14.2107 15.4696 14 16 14H18C18.5304 14 19.0391 14.2107 19.4142 14.5858C19.7893 14.9609 20 15.4696 20 16V18C20 18.5304 19.7893 19.0391 19.4142 19.4142C19.0391 19.7893 18.5304 20 18 20H16C15.4696 20 14.9609 19.7893 14.5858 19.4142C14.2107 19.0391 14 18.5304 14 18V16Z" stroke="#63A848" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                      </svg>
                      <div className="text-base leading-6 font-medium text-gray-900">
                        About us
                      </div>
                    </a>
                  </Link>
                  <Link href="/#" >
                    <a className="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150" onClick={navToggle}>
                      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20.7107 12.1256H19.4867C19.4867 8.13102 16.2378 4.88287 12.2454 4.88287L12.2447 3.66032C16.9137 3.65812 20.7107 7.45656 20.7107 12.1256ZM12.2447 5.77187L12.2439 6.99443C15.0748 6.99443 17.3744 9.29627 17.3729 12.1249L18.597 12.1242C18.597 8.62181 15.7492 5.77187 12.2447 5.77187ZM12 0C5.38248 0 0 5.38248 0 12.0007C0 18.6175 5.38248 24 12 24C12.4092 24 12.7399 23.6694 12.7399 23.2609C12.7399 22.8502 12.4092 22.5188 12 22.5188C6.19874 22.5188 1.48044 17.7991 1.48044 12.0007C1.48044 6.19947 6.19874 1.48044 12 1.48044C17.8013 1.48044 22.5188 6.20021 22.5188 12.0007C22.5188 14.4245 21.2287 16.8263 19.3824 17.8446C18.7116 18.2134 17.9842 18.3964 17.2084 18.4037C17.6742 18.1267 18.0775 17.752 18.3773 17.2804C18.436 17.1915 18.5139 17.1128 18.5521 17.0166C18.7931 16.4369 18.8144 15.7793 18.9599 15.1666C19.1384 14.3849 15.5376 12.8574 15.2246 13.8338C15.1115 14.1953 14.9381 15.3385 14.7111 15.6397C14.509 15.9087 14.0109 15.7808 13.6994 15.5163C12.8787 14.8169 11.9625 13.789 11.1588 12.9764L11.1602 12.9757C11.1397 12.9544 11.1147 12.9309 11.0926 12.9081C11.0699 12.8868 11.0471 12.8625 11.0243 12.8405V12.842C10.2117 12.0367 9.18313 11.1228 8.48442 10.3006C8.21992 9.9891 8.09208 9.49097 8.36099 9.28892C8.66222 9.06043 9.80542 8.88998 10.1669 8.77536C11.1411 8.46385 9.61587 4.86083 8.83267 5.0401C8.22139 5.18337 7.56383 5.20688 6.98414 5.4464C6.88569 5.48828 6.80928 5.56395 6.72038 5.62273C4.66393 6.9261 4.3987 10.0912 6.40593 12.4496C7.17076 13.3519 7.96865 14.2247 8.79006 15.077L8.78565 15.0799C8.80842 15.1027 8.83194 15.124 8.85545 15.146C8.87749 15.1688 8.89879 15.1916 8.9201 15.2158L8.92451 15.2114C9.77677 16.0328 10.787 17.2429 12.4283 18.3376C15.8837 20.6453 18.5352 20.001 20.0972 19.1443C22.7936 17.6595 24 14.4877 24 12.0037C24 5.38248 18.6168 0 12 0Z" fill="#63A848"/>
                      </svg>

                      <div className="text-base leading-6 font-medium text-gray-900">
                      Contact us
                      </div>
                    </a>
                  </Link>
                  </nav>
                </div>
              </div>
              <div className="py-6 px-5 space-y-6">
            
                <div className="space-y-6 mobile_auth">
                
                  <Link href="/login/options">
                    <a className="block text-center w-full sign_up whitespace-no-wrap focus:outline-none focus:text-gray-900">
                    Log in
                    </a>
                  </Link>
                  {/* <p className="text-center text-base leading-6 font-medium text-gray-500">
                    Existing customer? &nbsp;
                    <Link href="/login/options">
                    <a className=" whitespace-no-wrap focus:outline-none focus:text-gray-900">
                    Log in
                    </a>
                  </Link>
                  </p> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
    
  )
}
