import Navbar from './Navbar';
const NavLayout =({children})=>{
    return(
        <>
            <Navbar/>
            {children}
        </>
    )
}
export default NavLayout;