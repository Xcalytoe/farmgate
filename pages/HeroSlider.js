import Carousel from "react-multi-carousel";
import Link from "next/link";
import "react-multi-carousel/lib/styles.css";

const HeroSlider = ()=>{
    const responsive = {
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 1,
          slidesToSlide: 1 // optional, default to 1.
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 1,
          slidesToSlide: 1 // optional, default to 1.
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1,
          slidesToSlide: 1 // optional, default to 1.
        }
      };
    return(
        <>
            <Carousel
            swipeable={false}
            draggable={false}
            showDots={true}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            // autoPlay={this.props.deviceType !== "mobile" ? true : false}
            autoPlay={true}
            autoPlaySpeed={1000}
            keyBoardControl={true}
            customTransition="all 0.5 ease-in-out"
            // customTransition="transform 300ms ease-in-out"
            transitionDuration={5000}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            // deviceType={this.props.deviceType}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
            >
                <div className="relative hero_container ">
                    <div className="absolute img_hero">
                        <img className="" src="/images/slider/slide1.jpg"/>
                    </div>
                    <div className="hero_grid footer_container h-full grid items-center">
                        <div className="hero_text">
                            <h3 className="text-white">Access to all your needs and more in the <span>Food Value Chain</span></h3>
                            <p>Farmgate leverages technology to connect the food value chain participants
with access to finance, reduce the cost of production, open market access and
harvest better yields - all in one place.</p>
                            <div>
                                {/* <Link href="/register/options"> */}
                                <Link href="/login/options">
                                    <a className="join_link">Login Now</a>
                                </Link>
                            </div>
                        </div>
                        <div className=" pt-4 md:pt-0">
                            <div>
                                {/* <img src="/images/slider/africa.svg" alt="Map of Africa"/> */}
                                <img src="/images/slider/globe.svg" alt="Map of Africa"/>

                            </div>
                        </div>
                    </div>
                </div>
                {/* <div>
                <img src="/images/slider/slide2.jpg"/>
                    
                </div> */}
                {/* <div>Item 3</div>
                <div>Item 4</div> */}
            </Carousel>
        </>
    )
}
export default HeroSlider;