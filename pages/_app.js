import '../styles/globals.css'
import '../styles/index.css'
import "../styles/progress_form.css";
import "../styles/sass/farmgate.scss";
import Head from 'next/head'

function MyApp({ Component, pageProps }) {
  const Layout = Component.Layout || emptyNavLayout;
  return (
    <>
      <Head>
        <link rel="preload"
  href="/fonts/stylesheet.css"
  as="font"
  type="font/woff2"></link>
      </Head>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
    )
}
// without navbar 
const emptyNavLayout = ({children})=><>{children}</>;

export default MyApp
