import Link from 'next/link';
const RegionDetails = ({regionClass}) =>{
    return(
        <> 
        {/* details_grid must have numbers attached */}
            <div className={"details_grid details_grid1 " + regionClass}>
            <div className="items_">
                    <h5>Farmer</h5>
                    <p>We are focused on helping farmers and low-income earners create and protect their wealth by leveraging technology.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Buyers</h5>
                    <p>We provide agrocommodity buyers and sellers access to superior insights, and better pricing across the entire agricultural supply chain.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Financiers</h5>
                    <p>We make available finance and Insurance options for crop and livestock inputs such as cassava, rice, wheat, tomatoes, maize, and others.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Input Providers</h5>
                    <p>Farmgate believes Improving access to high-quality agricultural inputs and services is key to increasing agricultural productivity and addressing food security challenges.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
            </div>
            {/* second region  */}
            <div className={"details_grid details_grid2 " + regionClass}>
                <div className="items_">
                    <h5>Farmer</h5>
                    <p>We are focused on helping farmers and low-income earners create and protect their wealth by leveraging technology.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Buyers</h5>
                    <p>We provide agrocommodity buyers and sellers access to superior insights, and better pricing across the entire agricultural supply chain.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Financiers</h5>
                    <p>We make available finance and Insurance options for crop and livestock inputs such as cassava, rice, wheat, tomatoes, maize, and others.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Input Providers</h5>
                    <p>Farmgate believes Improving access to high-quality agricultural inputs and services is key to increasing agricultural productivity and addressing food security challenges.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                
            </div>
                {/* third region  */}
                <div className={"details_grid details_grid3 " + regionClass}>
                <div className="items_">
                    <h5>Farmer</h5>
                    <p>We are focused on helping farmers and low-income earners create and protect their wealth by leveraging technology.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Buyers</h5>
                    <p>We provide agrocommodity buyers and sellers access to superior insights, and better pricing across the entire agricultural supply chain.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Financiers</h5>
                    <p>We make available finance and Insurance options for crop and livestock inputs such as cassava, rice, wheat, tomatoes, maize, and others.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Input Providers</h5>
                    <p>Farmgate believes Improving access to high-quality agricultural inputs and services is key to increasing agricultural productivity and addressing food security challenges.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                
            </div>
        </>
    )
}
export default RegionDetails;