import Carousel from "react-multi-carousel";
import Link from "next/link";
import "react-multi-carousel/lib/styles.css";

const Stories = ()=>{
    const responsive = {
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 4,
          slidesToSlide: 1 // optional, default to 1.
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 3,
          slidesToSlide: 1 // optional, default to 1.
        },
        tablet: {
            breakpoint: { max: 767, min: 464 },
            items: 2,
            slidesToSlide: 1 // optional, default to 1.
          },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1,
          slidesToSlide: 1 // optional, default to 1.
        }
      };
    return(
        <>
            <Carousel
            swipeable={false}
            draggable={true}
            showDots={false}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            // itemsToShowTimeout={true}
            // autoPlay={this.props.deviceType !== "mobile" ? true : false}
            autoPlay={true}
            autoPlaySpeed={1000}
            keyBoardControl={true}
            customTransition="all 0.5 ease-in-out"
            // customTransition="transform 300ms ease-in-out"
            transitionDuration={5000}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["desktop", "tablet", "mobile"]}
            // deviceType={this.props.deviceType}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
            >
               <div className="story_item mx-2 md:mr-3 md:ml-0">
                   <div className="user_img">
                       <img src="/images/segun.png" alt="User"/>
                   </div>
                    <p>With Farmgate, we can now operate at our full capacity of 20,000 birds. </p>
                    <p className="writer_">Adetunji Olusegun</p>
                    <p className="sub_footer">Poultry Farmer, Ogun State</p>
               </div>
               <div className="story_item mx-2 md:mr-3 md:ml-0">
                    <div className="user_img">
                       <img src="/images/patience.png" alt="User"/>
                   </div>
                    <p>With Farmgate, I was able to gather enough income for my school. </p>
                    <p className="writer_">Patience Akpan</p>
                    <p className="sub_footer">Farmer, Nigeria</p>
               </div>
               <div className="story_item mx-2 md:mr-3 md:ml-0">
                    <div className="user_img">
                       <img src="/images/Victoria.png" alt="User"/>
                   </div>
                    <p>I am a part of this exciting journey because it empowers and encourages farmers </p>
                    <p className="writer_">Victoria Etim Bassey</p>
                    <p className="sub_footer">Software Developer</p>
               </div>
               <div className="story_item mx-2 md:mr-3 md:ml-0">
                    <div className="user_img">
                       <img src="/images/ajibola.png" alt="User"/>
                   </div>
                    <p>If you’re not a farmer, then be a sponsor.</p>
                    <p className="writer_">Ajibola Oyekunle</p>
                    <p className="sub_footer">HR professional</p>
               </div>
              
            </Carousel>
        </>
    )
}
export default Stories;