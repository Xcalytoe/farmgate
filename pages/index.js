import Head from 'next/head'
import styles from '../styles/Home.module.css'
import NavLayout from '../components/navLayout';
import HeroSlider from './HeroSlider';
import Possibilities from './Possibilities';
import Region from './Region';
import Stories from './Stories';
import Partners from './Partners';


export default function Home() {
  return (
    <>
     <Head>
        <title>Farmgate</title>
        <link rel="icon" href="/farmgate_favicon.ico" />
        {/* <link
            rel="preload"
            href="/fonts/stylesheet.css"
            as="style"
            crossOrigin=""
          /> */}
      </Head>
      <section>
        <HeroSlider/>
      </section>
      <section style={{background:"#fefefe"}}>
        <div className=" footer_container container_pad">
          <div className="section_head head_container py-6 md:py-10">
            <h3>World of <span>Endless Possibilities</span></h3>
            <p>No matter what your requirement is in the food value chain, we have a solution for you</p>
          </div>
        </div>
       
      </section>
      <div className="possibilities">
         <div className="footer_container container_pad">
          <Possibilities/>
         </div>
      </div>
      <section id="region_section">
        <Region/>
      </section>
      <section className="stories_container">
        <div className="head_container stories_head footer_container container_pad">
          <h3>Success <span>Stories</span></h3>
          <p>What people are saying about Farrmgate.</p>
        </div>
        <div className="footer_container container_pad">
            <Stories/>
          </div>
      </section>
      <section className="partners_section">
        <div className="head_container footer_container text-center container_pad">
          <h3>Our  <span>Partners</span></h3>
          {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> */}
        </div>
        <div className="partner_container footer_container container_pad">
          <Partners/>
        </div>
      </section>
      <section className="seo_bg" id="about_us_section">
          <div className="head_container footer_container container_pad">
            <h3>Farmgate</h3>
            <p>Farmgate leverages technology to connect the food value chain participants with access to finance, reduce the cost of production, open market access and harvest better yields - all in one place. We make available finance and Insurance options for crop and livestock inputs such as cassava, rice, wheat, tomatoes, maize, and others through CBN Anchor Borrowers Programme, Agriculture for Food and Jobs Plan (AFJP), and other intervention schemes. We are focused on helping farmers and low-income earners create and protect their wealth by leveraging technology. Farmgate provides access to over 424,966 seamless supply of agricultural inputs and produce nationwide making input disbursement and produce procurement easy and fast. We are committed to growth. Farmgate connects buyers to competitive offers from verified suppliers like you. We provide agrocommodity buyers and sellers access to superior insights, and better pricing across the entire agricultural supply chain. Leverage the opportunity to gain a deep understanding of the market in real-time for the right investment opportunity. Create projects to foster the right growth opportunities for farmers in the agricultural value chain through our structured finance. Gain key insights to your investments performance through technology and the leverage on the knowledge for structured alignment. Farmgate believes Improving access to high-quality agricultural inputs and services is key to increasing agricultural productivity and addressing food security challenges.</p>
          </div> 
      </section>
      <footer className="text-center footer_container">
        <p>© Farmgate 2021 - A product of Farmcrowdy. All rights reserved.</p>
      </footer>
    </>
  )
}
Home.Layout = NavLayout;
