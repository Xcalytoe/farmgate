import {useState} from 'react';
import Basic from './Basic';
import Address from './Address';
import Review from './Review';

const BuyerControl = ({ changeStateOne, changeStateTwo, changeStateThree}) =>{
            //  setProgressPropsFunctions 
    // const setProgressPropsFunctions = {changeStateOne, changeStateTwo, changeStateThree};
    // form input state
    const [step, setStep] = useState(1);
    const [address, setAddress] = useState("");
    const [postalCode, setPostalCode] = useState("");
    const [city, setCity] = useState("");
    const [region, setRegion] = useState("");
    const [secInput, setSecInput ] = useState("");
    const [input, setInput] = useState("");
    const [phoneNo, setPhoneNo] = useState("");
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastname] = useState("");
  
    const FormItem = {address, setAddress, postalCode, setPostalCode, city, setCity, region, setRegion, secInput, setSecInput, lastName, setLastname, firstName, setFirstName, email, setEmail, input, setInput, phoneNo, setPhoneNo, changeStateOne, changeStateTwo, changeStateThree};
    
      // Go to next step
  const nextStep = () => {
    setStep(step + 1);
  };

  // Go to prev step
  const prevStep = () => {
    // const { step } = this.state;
    setStep(step - 1);
  };
//   back to first step 
    const firstStep = () => {
    setStep(1);
  };
  //   back to second step 
  const secondStep = () => {
    setStep(2);
  };
//     return(
//         <>
//  <Head>
//                 <title> Agro Buyer Registration - Farmgate</title>
//                 <link rel="icon" href="/farmgate_favicon.ico" />
//                 {/* <link
//                     rel="preload"
//                     href="/fonts/stylesheet.css"
//                     as="style"
//                     crossOrigin=""
//                 /> */}
//             </Head>
//             {/* <BuyerProvider> */}
//                 <div className="footer_container form_main_container">
//                     <RegNav/>
//                     <div className="title_ py-7">
//                         <h4 className="text-2xl sm:text-3xl md:text-4xl sm:leading-10 md:leading-snug">Create your Farmgate Account</h4>
//                     </div>
//                     {/* form progress  */}
//                    <ProgressBar {...progressProps}/>
//                 </div>  
//                 <div>
                switch (step) {
     
                case 1:
                    return (
                        <Basic 
                            {...FormItem}
                            nextStep={nextStep}
                            // handleChange={handleChange}
                        />
                    //   <FormUserDetails
                    //     nextStep={this.nextStep}
                    //     handleChange={this.handleChange}
                    //     values={values}
                    //   />
                    );
                case 2:
                    return (
                        <Address
                            {...FormItem}
                            nextStep={nextStep}
                            prevStep={prevStep}
                        />
                    //   <FormPersonalDetails
                    //     nextStep={this.nextStep}
                    //     prevStep={this.prevStep}
                    //     handleChange={this.handleChange}
                    //     values={values}
                    //   />
                    );
                case 3:
                    return (
                        <Review
                            {...FormItem}
                            firstStep={firstStep}
                            secondStep={secondStep}
                        />
                    //   <Confirm
                    //     nextStep={this.nextStep}
                    //     prevStep={this.prevStep}
                    //     values={values}
                    //   />
                    );
                    default:
                        return null;
                //
                //             </div>
                //     </>
                // )
            }
}
export default BuyerControl;