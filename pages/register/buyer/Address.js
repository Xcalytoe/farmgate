
// note that you can also export the source data via CountryRegionData. It's in a deliberately concise format to 
// keep file size down
import { RegionDropdown } from 'react-country-region-selector';
// import NaijaStates from 'naija-state-local-government';
import ItemForm from "../../input/ItemForm";

const Address = ({address, setAddress, postalCode, setPostalCode, city, setCity, region, setRegion, nextStep, prevStep, changeStateOne, changeStateTwo, changeStateThree}) => {
// destructure the setProgressPropsFunctions 
// const {changeStateOne, changeStateTwo, changeStateThree} = setProgressPropsFunctions;
  const prevFunction = () =>{
    changeStateOne("_35Ago");
    changeStateTwo("");
    prevStep();
  }
  const nextFunction = () =>{
    changeStateTwo("_2ZUAI");
    changeStateThree("_35Ago");
    // changeStateOne("");
    nextStep();
  }
  return (
    <div className="form_step">
      <h5>Location Delivery Details</h5>
      <div className="flex flex-wrap md:space-x-8 pt-3">
        <div className="form_container flex-grow">
          <label className="block">State</label>
          <RegionDropdown className="w-full"
            country="Nigeria"
            defaultOptionLabel="Select State"
            value={region}
            onChange={(val) => setRegion(val)} />
        </div>
        <ItemForm label="City" name="city" value={city} type="text" placeholder="Enter city" onChange={(e)=>setCity(e.target.value)} />
        {/* <div className="form_container flex-grow">
          <label className="block">What primary input you would like to buy?</label>
          <select className="w-full" >
            {myLGAs}
          </select>
        </div> */}
      </div>
      <div className="flex flex-wrap md:space-x-8 pt-3">
        <div className="form_container flex-grow">
          <label className="block">Address</label>
          <textarea value={address} onChange={(e)=>setAddress(e.target.value)} className="w-full" placeholder="Fill in the street name, house number, complex number, building name, floor or plot  number and a landmark"></textarea>
        </div>
        <div className="form_container flex-grow">
        
        </div>
      </div>
      <div className="flex flex-wrap md:space-x-8 pt-3">
        <ItemForm label="Postal code" placeholder="Postal code" name="postalCode" type="number" value={postalCode} onChange={(e)=>setPostalCode(e.target.value)} />
        <div className="form_container flex-grow">
        
        </div>
      </div>
      <div className="flex justify-between my-8">
        <button className="backBtn_" onClick={prevFunction}>
          <svg className="inline-block mr-2 relative" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8.31906 3.92252C8.56875 3.6847 8.96469 3.69408 9.2025 3.94408C9.44063 4.19408 9.43063 4.5897 9.18094 4.82783L4.38656 9.37502H16.875C17.22 9.37502 17.5 9.65502 17.5 10C17.5 10.345 17.22 10.625 16.875 10.625H4.40656L9.18094 15.1725C9.43094 15.4106 9.44063 15.8063 9.2025 16.0563C9.07969 16.185 8.915 16.25 8.75 16.25C8.595 16.25 8.44 16.1928 8.31906 16.0775L2.86625 10.8838C2.63 10.6478 2.5 10.3341 2.5 10C2.5 9.66595 2.63 9.3522 2.87719 9.10564L8.31906 3.92252Z" fill="white"/>
          </svg>

          Previous
          </button>
        <button className="nextBtn" onClick={nextFunction}>Next
          <svg className="inline-block ml-2 relative" width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.68094 0.922515C9.43125 0.684703 9.03531 0.694078 8.7975 0.944078C8.55937 1.19408 8.56937 1.5897 8.81906 1.82783L13.6134 6.37502H1.125C0.78 6.37502 0.5 6.65502 0.5 7.00002C0.5 7.34501 0.78 7.62501 1.125 7.62501H13.5934L8.81906 12.1725C8.56906 12.4106 8.55937 12.8063 8.7975 13.0563C8.92031 13.185 9.085 13.25 9.25 13.25C9.405 13.25 9.56 13.1928 9.68094 13.0775L15.1337 7.88376C15.37 7.64783 15.5 7.33408 15.5 7.00002C15.5 6.66595 15.37 6.3522 15.1228 6.10564L9.68094 0.922515Z" fill="white"/>
          </svg>
        </button>
      </div>
    </div>
  );
};

export default Address;