import ItemForm from "../../input/ItemForm";
import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input'
const Basic = ({ funderType, setFunderType, fPhoneNo, bvn, setBvn, setFPhoneNo, fEmail, fFirstName, setFFirstName, setFEmail, interest, fOrganisationName, setFOrganisationName, setInterest, changeStateOne, fLastName, setFLastName, nextStep, changeStateTwo, changeStateThree}) => {
  // destructure the setProgressPropsFunctions 
// const {changeStateOne, changeStateTwo, changeStateThree} = setProgressPropsFunctions;
  const nextFunction = () =>{
    changeStateOne("_2ZUAI");
    changeStateTwo("_35Ago")
    // changeStateOne("");
    nextStep();
  }

  
  return (
    <div className="form_step">
        <h5>Farmer Details</h5>
      <div className="flex flex-wrap md:space-x-8 pt-3">
          <div className="form_container flex-grow">
              <ItemForm
                label="First Name"
                name="firstName"
                value={fFirstName}
                type = "text"
                placeholder="Johnny"
                onChange={(e)=>setFFirstName(e.target.value)}
              />
          </div>
          <div className="form_container flex-grow">
            <ItemForm
              label="Last Name"
              name="lastName"
              value={fLastName}
              type = "text"
              placeholder="Doe"
              onChange={(e)=>setFLastName(e.target.value)}
            />
          </div>
      </div>
      <div className="flex flex-wrap md:space-x-8 pt-3">
        <ItemForm
            label="Email"
            name="email"
            value={fEmail}
            type = "email"
            placeholder="Johnnydoe@gmail.com"
            onChange={(e)=>setFEmail(e.target.value)}
          />
        <div className="form_container flex-grow">
          <label className="block">Gender</label>
         <select className="w-full">
           <option>Select gender</option>
           <option value="Male">Male</option>
           <option value="Female">Female</option>
         </select>
        </div>
      </div>
      <div className="flex flex-wrap md:space-x-8 pt-3">
      
        <div className="form_container flex-grow">
          <label className="block">Mobile</label>
          <PhoneInput
          international
          defaultCountry="NG"
          value={fPhoneNo}
          onChange={setFPhoneNo}/>
        </div>
        <ItemForm
            label="BVN"
            name="bvn"
            value={bvn}
            type = "number"
            placeholder="23xxxxx"
            onChange={(e)=>setBvn(e.target.value)}
          />
      </div>
      <div className="my-8 text-right">
        <button className="nextBtn" onClick={nextFunction}>Next 
          <svg className="inline-block ml-2 relative" width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.68094 0.922515C9.43125 0.684703 9.03531 0.694078 8.7975 0.944078C8.55937 1.19408 8.56937 1.5897 8.81906 1.82783L13.6134 6.37502H1.125C0.78 6.37502 0.5 6.65502 0.5 7.00002C0.5 7.34501 0.78 7.62501 1.125 7.62501H13.5934L8.81906 12.1725C8.56906 12.4106 8.55937 12.8063 8.7975 13.0563C8.92031 13.185 9.085 13.25 9.25 13.25C9.405 13.25 9.56 13.1928 9.68094 13.0775L15.1337 7.88376C15.37 7.64783 15.5 7.33408 15.5 7.00002C15.5 6.66595 15.37 6.3522 15.1228 6.10564L9.68094 0.922515Z" fill="white"/>
          </svg>

        </button>
      </div>
    </div>
  );
};

export default Basic;