import {useState} from 'react';
import Basic from './Basic';
import Details from './Details';
import LandDetails from './LandDetails';
import Review from './Review';

const FinancierControl = ({ changeStateOne, changeStateTwo, changeStateThree, changeStateFour}) =>{
            //  setProgressPropsFunctions 
    // const setProgressPropsFunctions = {changeStateOne, changeStateTwo, changeStateThree};
    // form input state
    const [step, setStep] = useState(1);
    const [city, setCity] = useState("");
    const [fPhoneNo, setFPhoneNo] = useState("");
    const [fEmail, setFEmail] = useState("");
    const [fFirstName, setFFirstName] = useState("");
    const [fLastName, setFLastName] = useState("");
    const [bvn, setBvn] = useState("");
    const [fRegion, setFRegion] = useState("");
    const [landSize, setLandSize] = useState("");
    const [ lAddress, setLAddress] = useState("");
    const [postalCode, setPostalCode] = useState("")
    const [busName, setBusName] = useState("")
    const [orgYear, setOrgYear] = useState("")
    const [orgMembers, setOrgMembers] = useState("")
    const [painProduct, setPainProduct] = useState("")
    const [farmAge, setFarmAge] = useState("");
    const [agricCertificate, setAgricCertificate] = useState("hasCertificate");
      // collect all input states 
    const [addDataForm, setAddDataForm] = useState({farmerStatus:"individualFarmer"});
    // state for duplicate form field 
    const [addDataState, setAddDataState] = useState(1);
// hide form field section for a different farmer status 
    const [hideField, setHideField] = useState("hidden");
    const [fOrganisationName, setFOrganisationName] = useState("");
  
    const FormItem = {city, setCity, fPhoneNo, setFPhoneNo, fEmail, fFirstName, setFFirstName, setFEmail, addDataState, setAddDataState, hideField, setHideField, agricCertificate, setAgricCertificate, farmAge, setFarmAge, busName, setBusName, orgYear, setOrgYear, orgMembers, setOrgMembers, painProduct, setPainProduct, lAddress, setLAddress, bvn, setBvn, addDataForm, setAddDataForm, fRegion, setFRegion, postalCode, setPostalCode, fOrganisationName, setFOrganisationName, landSize, setLandSize, changeStateOne, fLastName, setFLastName, changeStateTwo, changeStateThree, changeStateFour};
    
      // Go to next step
  const nextStep = () => {
    setStep(step + 1);
  };

  // Go to prev step
  const prevStep = () => {
    // const { step } = this.state;
    setStep(step - 1);
  };
//   back to first step 
    const firstStep = () => {
    setStep(1);
  };
  //   back to second step 
  const secondStep = () => {
    setStep(2);
  };
    //   back to third step 
    const thirdStep = () => {
      setStep(3);
    };
  switch (step) {

  case 1:
      return (
          <Basic 
              {...FormItem}
              nextStep={nextStep}
              // handleChange={handleChange}
          />
      );
  case 2:
      return (
          <Details
              {...FormItem}
              nextStep={nextStep}
              prevStep={prevStep}
          />

      );
      case 3:
        return (
            <LandDetails
                {...FormItem}
                nextStep={nextStep}
                prevStep={prevStep}
            />
  
        );
  case 4:
      return (
          <Review
              {...FormItem}
              firstStep={firstStep}
              secondStep={secondStep}
              thirdStep={thirdStep}
          />
      );
      default:
          return null;
  }
}
export default FinancierControl;