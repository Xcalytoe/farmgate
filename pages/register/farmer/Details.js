import ItemForm from "../../input/ItemForm";
const Details = ({  hideField, setHideField,  addDataForm, setAddDataForm, farmAge, setFarmAge, busName, setBusName, orgYear, setOrgYear, orgMembers, setOrgMembers, painProduct, setPainProduct, agricCertificate, setAgricCertificate, changeStateOne, prevStep, nextStep,  changeStateTwo, changeStateThree}) => {

  const prevFunction = () =>{
    changeStateOne("_35Ago");
    changeStateTwo("");
    prevStep();
  }
  const nextFunction = () =>{
    changeStateTwo("_2ZUAI");
    changeStateThree("_35Ago");
    // changeStateOne("");
    nextStep();
  }
  console.log(agricCertificate)
    // assign states to any of the selected radio
    const onChange = (e)=>{
      const inputId = e.target.id;
      const name = e.target.name;
      setAddDataForm({...addDataForm, [name]:inputId});
  }

  return (
    <div className="form_step">
      <h5>Farmer Data</h5>
      <p className="farmerStatusP mt-10">Status</p>
      <div className="flex flex-wrap md:space-x-8 py-3">
          <div className="farmerStatus">
            <input className="" name="farmerStatus" id="individualFarmer" type="radio"  onChange={(e)=>{setHideField("hidden"); onChange(e)}} value="individualFarmer" checked={addDataForm?.farmerStatus === "individualFarmer"}/>
            <label className="flex items-center" htmlFor="individualFarmer"><span></span>Individual Farmer</label>
          </div>
          <div className="farmerStatus">
            <input className="" name="farmerStatus" id="largeScale" type="radio" onChange={(e)=>{setHideField(""); onChange(e)}}  value="largeScale" checked={addDataForm?.farmerStatus === "largeScale"}/>
            <label className="flex items-center" htmlFor="largeScale"><span></span>Large Scale Farmer</label>
          </div>
          <div className="farmerStatus">
            <input className="" name="farmerStatus" id="associationFarmer" type="radio"  onChange={(e)=>{setHideField(""); onChange(e)}}  value="associationFarmer" checked={addDataForm?.farmerStatus === "associationFarmer"}/>
            <label className="flex items-center" htmlFor="associationFarmer"><span></span>Cooperative/Association Farmer</label>
          </div>
      </div>
      <div className="flex flex-wrap md:space-x-8 pt-3">
        <div className="form_container flex-grow">
          <label className="block">How old is your Farm?</label>
          <div className="flex short_flex items-center">
            <input className="w-full" min="0" type="number" name="farmAge" value={farmAge} onChange={(e)=>setFarmAge(e.target.value)} placeholder="e.g 10"/>
            <span className="flex">Year(s)</span>
          </div>
        </div>
        <div className="form_container  flex-grow"></div>
      </div>
      <div className={"mt-6 " + hideField}>
        <div className="flex flex-wrap md:space-x-8 pt-3">
          <ItemForm label="Business Name" name="organisationName" value={busName} type="text" placeholder="Write the name of the business" onChange={(e)=>setBusName(e.target.value)} />
          <ItemForm label="Year Organisation was founded" name="foundedYear" value={orgYear} type="number" placeholder="Write the age of your organisation " onChange={(e)=>setOrgYear(e.target.value)} />
        </div>
        <div className="flex flex-wrap md:space-x-8 pt-3">
          <ItemForm label="Number of members in your organisation" name="organisationMembers" value={orgMembers} type="number" placeholder="e.g 10" onChange={(e)=>setOrgMembers(e.target.value)} />         
          <div className="form_container flex-grow">
            <label className="block">What’s your pain point</label>
            <select className="w-full" name="painPoint" onChange={(e)=>setPainProduct(e.target.value)} value={painProduct}>
              <option>Select point</option>
              <option value="Money">Money</option>
            </select>
          </div>
        </div>
        <p className="farmCert mt-10">Do you an Agricultural Practice Certificate?</p>
        <div className="flex flex-wrap md:space-x-8 py-3">
          <div className="farmerStatus">
            <input className="" name="practiceCert" id="yesCert" type="radio" value="hasCertificate" onChange={e=>setAgricCertificate(e.target.value)} checked={agricCertificate === "hasCertificate"} />
            <label className="flex items-center" htmlFor="yesCert"><span></span>Yes</label>
          </div>
          <div className="farmerStatus">
            <input className="" name="practiceCert" id="noCert" type="radio" value="noCertificate" onChange={e=>setAgricCertificate(e.target.value)} checked={agricCertificate === "noCertificate"}/>
            <label className="flex items-center" htmlFor="noCert"><span></span>No</label>
          </div>
      </div>
      </div>
      <div className="flex justify-between my-8">
        <button className="backBtn_" onClick={prevFunction}>
          <svg className="inline-block mr-2 relative" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8.31906 3.92252C8.56875 3.6847 8.96469 3.69408 9.2025 3.94408C9.44063 4.19408 9.43063 4.5897 9.18094 4.82783L4.38656 9.37502H16.875C17.22 9.37502 17.5 9.65502 17.5 10C17.5 10.345 17.22 10.625 16.875 10.625H4.40656L9.18094 15.1725C9.43094 15.4106 9.44063 15.8063 9.2025 16.0563C9.07969 16.185 8.915 16.25 8.75 16.25C8.595 16.25 8.44 16.1928 8.31906 16.0775L2.86625 10.8838C2.63 10.6478 2.5 10.3341 2.5 10C2.5 9.66595 2.63 9.3522 2.87719 9.10564L8.31906 3.92252Z" fill="white"/>
          </svg>

          Previous
          </button>
        <button className="nextBtn" onClick={nextFunction}>Next
          <svg className="inline-block ml-2 relative" width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.68094 0.922515C9.43125 0.684703 9.03531 0.694078 8.7975 0.944078C8.55937 1.19408 8.56937 1.5897 8.81906 1.82783L13.6134 6.37502H1.125C0.78 6.37502 0.5 6.65502 0.5 7.00002C0.5 7.34501 0.78 7.62501 1.125 7.62501H13.5934L8.81906 12.1725C8.56906 12.4106 8.55937 12.8063 8.7975 13.0563C8.92031 13.185 9.085 13.25 9.25 13.25C9.405 13.25 9.56 13.1928 9.68094 13.0775L15.1337 7.88376C15.37 7.64783 15.5 7.33408 15.5 7.00002C15.5 6.66595 15.37 6.3522 15.1228 6.10564L9.68094 0.922515Z" fill="white"/>
          </svg>
        </button>
      </div>
    </div>
  );
};


export default Details;