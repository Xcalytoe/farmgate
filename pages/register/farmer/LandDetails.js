import ItemForm from "../../input/ItemForm";
import { RegionDropdown } from 'react-country-region-selector';
import NaijaStates from 'naija-state-local-government';
import {NgCities} from '../../../components/NgCities';

const LandDetails = ({fRegion, setFRegion, lAddress, setLAddress, city, setCity, addDataForm, setAddDataForm, postalCode, setPostalCode, addDataState, setAddDataState, landSize, setLandSize, prevStep, nextStep,  changeStateTwo, changeStateThree, changeStateFour}) => {

  // assign states to any of the fields 
  const onChange = (e)=>{
    const name = e.target.name;
    const value = e.target.value;
    setAddDataForm({...addDataForm, [name]:value})
}
// const {changeStateOne, changeStateTwo, changeStateThree} = setProgressPropsFunctions;
  const prevFunction = () =>{
    changeStateTwo("_35Ago");
    changeStateThree("");
    prevStep();
  }
  const nextFunction = () =>{
    changeStateThree("_2ZUAI");
    changeStateFour("_35Ago");
    // changeStateOne("");
    nextStep();
  }
  console.log(NgCities)
console.log(addDataForm)
    const fields = [];
    for(let i = 1; i <= addDataState; i++){
      // element to be duplicated 
      const farmData = <div key={"farmData" + i} className="flex flex-wrap md:space-x-8 space-x-4 pt-3 flex_three">
                        <ItemForm label="Commodities" placeholder="e.g  Onions" name={"commodities" + i} type="text" onChange={onChange} value={addDataForm["commodities" + i] || ""} />
                        <div className="form_container flex-grow">
                            <label className="block">Type</label>
                            <select className="w-full" name={"farmType" + i} onChange={onChange} value={addDataForm["farmType" + i] || ""}>
                              <option value="" >Select type</option>
                              <option value="Livestock" >Livestock</option>
                              <option value="Machinery">Machinery</option>
                              <option value="Fertizilers">Fertizilers</option>
                            </select>
                          </div>
                          <div className="form_container flex-grow">
                          <label className="block">Production Capacity</label>
                          <div className="flex short_flex items-center">
                            <input className="w-full" min="0" type="number" name={"productionCapacity" + i} value={addDataForm["productionCapacity" + i] || ""}  onChange={onChange} placeholder="e.g 100"/>
                            <span className="flex">{addDataForm["farmType" + i] ? addDataForm["farmType" + i] : "Livestock"}</span>
                          </div>
                        </div>
                      </div>
      fields.push(farmData);
    }
  
  
  // const localGovernments = NaijaStates.all();
  // diplay cities from a selected state 
  const mapCity = NgCities[fRegion];
  let getStates;
  fRegion ?
     getStates = mapCity.map((data, index)=>{
        return <option key={data.city + index}>{data.city}</option>;
      }) : getStates = <option>Select state first</option>;
  // const getStates = localGovernments.map((data)=>{
  //   console.log(data.state)
  //   return data.state;
  // })

  return (
    <div className="form_step">
      <h5>Land Details</h5>
      <div className="flex flex-wrap md:space-x-8 pt-3">
        <div className="form_container flex-grow">
          <label className="block">Land size</label>
          <div className="flex short_flex items-center">
            <input className="w-full" min="0" type="number" name="landSize" value={landSize} onChange={e=>setLandSize(e.target.value)} placeholder="e.g 100"/>
            <span className="flex">hectares</span>
          </div>
        </div>
        <div className="form_container flex-grow">
          <label className="block">State/Province</label>
          <RegionDropdown className="w-full"
            country="Nigeria"
            defaultOptionLabel="Select State"
            value={fRegion}
            onChange={(val) => {
              setFRegion(val);
            }} />
        </div>
      </div>
      <div className="flex flex-wrap md:space-x-8 pt-3">
        <div className="form_container flex-grow">
          <label className="block">City</label>
          <select className="w-full" value={city} onChange={e=>setCity(e.target.value)}>
            {/* hide field if state is not selected  */}
            {fRegion ? <option>Select City</option>: ""}
            {getStates}
          </select>
        </div>
        <ItemForm label="Postal code" min="0" placeholder="Postal code" name="postalCode" type="number" value={postalCode} onChange={(e)=>setPostalCode(e.target.value)} />
               
      </div>
      <div className="flex flex-wrap md:space-x-8 pt-3">
        <div className="form_container flex-grow">
          <label className="block">Address</label>
          <textarea value={lAddress} onChange={(e)=>setLAddress(e.target.value)} className="w-full" placeholder="Fill in the street name, house number, complex number, building name, floor or plot  number and a landmark"></textarea>
        </div>        
        <div className="form_container flex-grow">
        
        </div>
      </div>
      <h5>Farming Product Data </h5>
            {fields}
      <div className="addProduct my-12 pb-8">
        <button className="" onClick={()=>setAddDataState(addDataState + 1)}>Add Product</button>
      </div>
   
      <div className="flex justify-between my-8">
        <button className="backBtn_" onClick={prevFunction}>
          <svg className="inline-block mr-2 relative" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8.31906 3.92252C8.56875 3.6847 8.96469 3.69408 9.2025 3.94408C9.44063 4.19408 9.43063 4.5897 9.18094 4.82783L4.38656 9.37502H16.875C17.22 9.37502 17.5 9.65502 17.5 10C17.5 10.345 17.22 10.625 16.875 10.625H4.40656L9.18094 15.1725C9.43094 15.4106 9.44063 15.8063 9.2025 16.0563C9.07969 16.185 8.915 16.25 8.75 16.25C8.595 16.25 8.44 16.1928 8.31906 16.0775L2.86625 10.8838C2.63 10.6478 2.5 10.3341 2.5 10C2.5 9.66595 2.63 9.3522 2.87719 9.10564L8.31906 3.92252Z" fill="white"/>
          </svg>

          Previous
          </button>
        <button className="nextBtn" onClick={nextFunction}>Next
          <svg className="inline-block ml-2 relative" width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.68094 0.922515C9.43125 0.684703 9.03531 0.694078 8.7975 0.944078C8.55937 1.19408 8.56937 1.5897 8.81906 1.82783L13.6134 6.37502H1.125C0.78 6.37502 0.5 6.65502 0.5 7.00002C0.5 7.34501 0.78 7.62501 1.125 7.62501H13.5934L8.81906 12.1725C8.56906 12.4106 8.55937 12.8063 8.7975 13.0563C8.92031 13.185 9.085 13.25 9.25 13.25C9.405 13.25 9.56 13.1928 9.68094 13.0775L15.1337 7.88376C15.37 7.64783 15.5 7.33408 15.5 7.00002C15.5 6.66595 15.37 6.3522 15.1228 6.10564L9.68094 0.922515Z" fill="white"/>
          </svg>
        </button>
      </div>
    </div>
  );
};


export default LandDetails;