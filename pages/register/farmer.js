import FarmerControl from "./farmer/FarmerControl";
import React, {useState} from 'react';
import RegNav from './RegNav';
import Head from 'next/head';
import ProgressBar from "./farmer/ProgressBar";
const FarmerReg = () =>{
        // progress state 
        const [firstLi, setFirstLi] = useState("_35Ago");
        const [secondLi, setSecondLi] = useState("");
        const [thirdLi, setThirdLi] = useState("");
        const [fourthLi, setFourthLi] = useState("");
        const progressProps = {firstLi, secondLi, thirdLi, fourthLi};
    return(
        <>
            <Head>
                <title> Farmer Registration - Farmgate</title>
                <link rel="icon" href="/farmgate_favicon.ico" />
                {/* <link
                    rel="preload"
                    href="/fonts/stylesheet.css"
                    as="style"
                    crossOrigin=""
                /> */}
            </Head>
            {/* <React.StrictMode>
            <BuyerProvider> */}
                <div className="footer_container form_main_container">
                    <RegNav/>
                    <div className="title_ py-7">
                        <h4 className="text-2xl sm:text-3xl md:text-4xl sm:leading-10 md:leading-snug">Create your Farmgate Account</h4>
                    </div>
                    {/* form progress  */}
                    <ProgressBar {...progressProps}/>
                    <FarmerControl changeStateOne={buyerOne=>setFirstLi(buyerOne)} changeStateTwo={(buyerTwo)=>setSecondLi(buyerTwo)} changeStateThree={(buyerThree)=>setThirdLi(buyerThree)} changeStateFour={(farmerFour)=>setFourthLi(farmerFour)}/>                
                </div>
            {/* </BuyerProvider>
            </React.StrictMode> */}
        </>
    )
}
export default FarmerReg;