
import ItemForm from "../../input/ItemForm";
import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input'
const Basic = ({ iLastName, setILastname, iFirstName, setIFirstName, iEmail, setIEmail, iPhoneNo, setIPhoneNo, organisationName, iGender, setIGender, setOrganisationName, nextStep, changeStateOne, changeStateTwo, changeStateThree}) => {
  // destructure the setProgressPropsFunctions 
// const {changeStateOne, changeStateTwo, changeStateThree} = setProgressPropsFunctions;
  const nextFunction = () =>{
    changeStateOne("_2ZUAI");
    changeStateTwo("_35Ago")
    // changeStateOne("");
    nextStep();
  }
  // const [phoneNo, setPhoneNo, gender, setGender] = useContext(InputProviderRegContext);
  return (
    <div className="form_step">
        <h5>Input Provider Details</h5>
      <div className="flex flex-wrap md:space-x-8 pt-3">
          <div className="form_container flex-grow">
              <ItemForm
                label="First Name"
                name="firstName"
                value={iFirstName}
                type = "text"
                placeholder="Johnny"
                onChange={(e)=>setIFirstName(e.target.value)}
              />
          </div>
          <div className="form_container flex-grow">
            <ItemForm
              label="Last Name"
              name="lastName"
              value={iLastName}
              type = "text"
              placeholder="Doe"
              onChange={(e)=>setILastname(e.target.value)}
            />
          </div>
      </div>
      <div className="flex flex-wrap md:space-x-8 pt-3">
        <div className="form_container flex-grow">
          <label className="block">Mobile</label>
          <PhoneInput
          international
          defaultCountry="NG"
          value={iPhoneNo}
          onChange={setIPhoneNo}/>
        </div>
          <ItemForm
            label="Email"
            name="email"
            value={iEmail}
            type = "email"
            placeholder="Johnnydoe@gmail.com"
            onChange={(e)=>setIEmail(e.target.value)}
          />
      </div>
      <div className="flex flex-wrap md:space-x-8 pt-3">
      <div className="form_container flex-grow">
          <label className="block">Gender</label>
          <select className="w-full" onChange={(e)=>setIGender(e.target.value)} value={iGender}>
            <option>Select gender</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
        </div>
          <ItemForm
            label="Email"
            name="email"
            value={iEmail}
            type = "email"
            placeholder="Johnnydoe@gmail.com"
            onChange={(e)=>setIEmail(e.target.value)}
          />
      </div>
      {/* <div className="flex flex-wrap md:space-x-8 pt-3">
        <div className="form_container flex-grow">
          <label className="block">What primary input you would like to buy?</label>
          <select className="w-full" onChange={(e)=>setInput(e.target.value)} value={input}>
            <option>Select input type</option>
            <option value="Grains">Grains</option>
            <option value="Machinery">Machinery</option>
            <option value="Fertizilers">Fertizilers</option>
          </select>
        </div>
        <div className="form_container flex-grow">
          <label className="block">What primary input you would like to buy?</label>
          <select className="w-full" onChange={(e)=>setSecInput(e.target.value)} value={secInput}>
            <option>Select input type</option>
            <option value="Grains">Grains</option>
            <option value="Machinery">Machinery</option>
            <option value="Fertizilers">Fertizilers</option>
          </select>
        </div>
      </div> */}
     <div className="my-8 text-right">
        <button className="nextBtn" onClick={nextFunction}>Next 
          <svg className="inline-block ml-2 relative" width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.68094 0.922515C9.43125 0.684703 9.03531 0.694078 8.7975 0.944078C8.55937 1.19408 8.56937 1.5897 8.81906 1.82783L13.6134 6.37502H1.125C0.78 6.37502 0.5 6.65502 0.5 7.00002C0.5 7.34501 0.78 7.62501 1.125 7.62501H13.5934L8.81906 12.1725C8.56906 12.4106 8.55937 12.8063 8.7975 13.0563C8.92031 13.185 9.085 13.25 9.25 13.25C9.405 13.25 9.56 13.1928 9.68094 13.0775L15.1337 7.88376C15.37 7.64783 15.5 7.33408 15.5 7.00002C15.5 6.66595 15.37 6.3522 15.1228 6.10564L9.68094 0.922515Z" fill="white"/>
          </svg>

        </button>
      </div>
    </div>
  );
};

export default Basic;