import {useRouter} from 'next/router';

const Review = ({ iLastName, iFirstName, iEmail, iPhoneNo, organisationName, firstStep, secondStep, changeStateOne, changeStateTwo, changeStateThree }) => {
  const router = useRouter();
  const backToFirst = () =>{
    changeStateOne("_35Ago"); 
    changeStateTwo(""); 
    changeStateThree(""); 
    firstStep();
  }
  const backToSecond = () =>{
    changeStateThree(""); 
    changeStateTwo("_35Ago");
    secondStep();
  }
  const handleSubmit = () =>{
    router.push('/register/inputProvider/Successful');
  }

  return (
    <div className="form_step">
      <h5 className="pb-4">Review your data</h5>
      <div className="flex justify-between border-b pb-3 pt-4 edit_container mb-6">
        <h6> Details</h6>
          <button onClick={backToFirst}>
          <svg className="inline-block ml-2" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clipPath="url(#clip0)">
            <path d="M16.4283 10C16.0338 10 15.7141 10.3198 15.7141 10.7143V17.8571C15.7141 18.2515 15.3943 18.5713 14.9998 18.5713H2.14283C1.74833 18.5713 1.42854 18.2515 1.42854 17.8571V3.57154C1.42854 3.17704 1.74833 2.85725 2.14283 2.85725H10.7141C11.1086 2.85725 11.4284 2.53746 11.4284 2.14296C11.4284 1.74846 11.1086 1.42871 10.7141 1.42871H2.14283C0.959376 1.42871 0 2.38809 0 3.57154V17.8571C0 19.0405 0.959376 19.9999 2.14283 19.9999H14.9998C16.1833 19.9999 17.1426 19.0405 17.1426 17.8571V10.7143C17.1426 10.3198 16.8228 10 16.4283 10Z" fill="#E69740"/>
            <path d="M19.1993 0.80113C18.6865 0.28819 17.9909 8.01842e-05 17.2655 0.000163888C16.5398 -0.00192872 15.8434 0.286683 15.332 0.80159L5.92349 10.2093C5.84544 10.2879 5.78655 10.3835 5.75136 10.4886L4.32281 14.7742C4.19814 15.1485 4.40049 15.553 4.77478 15.6776C4.84739 15.7018 4.92344 15.7141 4.99994 15.7142C5.07661 15.7141 5.15283 15.7018 5.22565 15.6778L9.51132 14.2493C9.61657 14.2141 9.71221 14.1549 9.7906 14.0764L19.1991 4.66797C20.2669 3.60024 20.267 1.86899 19.1993 0.80113Z" fill="#E69740"/>
            </g>
            <defs>
            <clipPath id="clip0">
            <rect width="20" height="20" fill="white"/>
            </clipPath>
            </defs>
          </svg>
        </button>
      </div>
      <div className="output_">
   
        <p><span>First name:</span> {iFirstName}</p>
        <p><span>Last Name:</span> {iLastName}</p>
        <p><span>Phone no:</span> {iPhoneNo}</p>
        <p><span>Email:</span> {iEmail}</p>

      </div>
      <div className="flex justify-between border-b pb-3 pt-4 edit_container mb-6">
        <h6> Address</h6>
            <button onClick={backToSecond}>
          <svg className="inline-block ml-2" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clipPath="url(#clip0)">
            <path d="M16.4283 10C16.0338 10 15.7141 10.3198 15.7141 10.7143V17.8571C15.7141 18.2515 15.3943 18.5713 14.9998 18.5713H2.14283C1.74833 18.5713 1.42854 18.2515 1.42854 17.8571V3.57154C1.42854 3.17704 1.74833 2.85725 2.14283 2.85725H10.7141C11.1086 2.85725 11.4284 2.53746 11.4284 2.14296C11.4284 1.74846 11.1086 1.42871 10.7141 1.42871H2.14283C0.959376 1.42871 0 2.38809 0 3.57154V17.8571C0 19.0405 0.959376 19.9999 2.14283 19.9999H14.9998C16.1833 19.9999 17.1426 19.0405 17.1426 17.8571V10.7143C17.1426 10.3198 16.8228 10 16.4283 10Z" fill="#E69740"/>
            <path d="M19.1993 0.80113C18.6865 0.28819 17.9909 8.01842e-05 17.2655 0.000163888C16.5398 -0.00192872 15.8434 0.286683 15.332 0.80159L5.92349 10.2093C5.84544 10.2879 5.78655 10.3835 5.75136 10.4886L4.32281 14.7742C4.19814 15.1485 4.40049 15.553 4.77478 15.6776C4.84739 15.7018 4.92344 15.7141 4.99994 15.7142C5.07661 15.7141 5.15283 15.7018 5.22565 15.6778L9.51132 14.2493C9.61657 14.2141 9.71221 14.1549 9.7906 14.0764L19.1991 4.66797C20.2669 3.60024 20.267 1.86899 19.1993 0.80113Z" fill="#E69740"/>
            </g>
            <defs>
            <clipPath id="clip0">
            <rect width="20" height="20" fill="white"/>
            </clipPath>
            </defs>
          </svg>
        </button>
      </div>
      <div className="output_">
        <p><span>State:</span> {organisationName}</p>
      </div>
      <div className="my-8 text-right">
  {/* if(res.status === 200){Router.push('/dash')} */}
        <button className="nextBtn" onClick={handleSubmit}>Submit</button>
      </div>
    </div>
  );
};

export default Review;