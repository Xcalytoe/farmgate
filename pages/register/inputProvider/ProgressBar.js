
const ProgressBar = ({firstLi, secondLi, thirdLi}) =>{


    const spanClassOne = ()=>{
        if(firstLi == "_35Ago"){
            // return "_2kL0S";
            return (<li className="_2Jtxm _35Ago "><span className="_2kL0S">1</span><div className="_1hzhf ">Step 1</div></li>);
        }else if(firstLi == "_2ZUAI"){
            // return "_2JvrO";
            return (<li className="_2Jtxm _2ZUAI "><span className="_2JvrO"><svg width="1.5rem" viewBox="0 0 13 9" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 3.5L4.5 7.5L12 1" stroke="white" strokeWidth="1.5"></path></svg></span><div className="_1hzhf ">Step 1</div></li>);
        }else{
            return (<li className="_2Jtxm _1CcaK "><span className="_2JvrO">!</span><div className="_1hzhf ">Step 1</div></li>);
        }
    }
    const spanClassTwo = ()=>{
        if(secondLi == ""){
            // return "_2kL0S";
            return (<li className="_2Jtxm "><span className="_2kL0S">2</span><div className="_1hzhf ">Step 2</div></li>);
        }else if(secondLi == "_35Ago"){
            // return "_2JvrO";
            return (<li className="_2Jtxm _35Ago "><span className="_2kL0S">2</span><div className="_1hzhf ">Step 2</div></li>);
        }else if(secondLi == "_2ZUAI"){
            return (<li className="_2Jtxm _2ZUAI "><span className="_2JvrO"><svg width="1.5rem" viewBox="0 0 13 9" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 3.5L4.5 7.5L12 1" stroke="white" strokeWidth="1.5"></path></svg></span><div className="_1hzhf ">Step 2</div></li>);
        }else{
            return(<li className="_2Jtxm _1CcaK "><span className="_2JvrO">!</span><div className="_1hzhf ">Step 2</div></li>);
        }
    }
    const spanClassThree = ()=>{
        if(thirdLi == ""){
            // return "_2kL0S";
            return (<li className="_2Jtxm "><span className="_2kL0S">3</span><div className="_1hzhf ">Step 3</div></li>);
        }else if(thirdLi == "_35Ago"){
            // return "_2JvrO";
            return (<li className="_2Jtxm _35Ago "><span className="_2kL0S">3</span><div className="_1hzhf ">Step 3</div></li>);
        }else if(thirdLi == "_2ZUAI"){
            return (<li className="_2Jtxm _2ZUAI "><span className="_2JvrO"><svg width="1.5rem" viewBox="0 0 13 9" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 3.5L4.5 7.5L12 1" stroke="white" strokeWidth="1.5"></path></svg></span><div className="_1hzhf ">Step 3</div></li>);
        }else{
            return(<li className="_2Jtxm _1CcaK "><span className="_2JvrO">!</span><div className="_1hzhf ">Step 3</div></li>);
        }
    }
    return(

        <div className="pb-10">
            <ul className="_1Lo2h ">
                {spanClassOne()}
                {spanClassTwo()}
                {spanClassThree()}
            </ul>
        </div>
    )
}
export default ProgressBar;