import {useState} from 'react';
import Basic from './Basic';
import Details from './Details';
import Review from './Review';

const FinancierControl = ({ changeStateOne, changeStateTwo, changeStateThree}) =>{
            //  setProgressPropsFunctions 
    // const setProgressPropsFunctions = {changeStateOne, changeStateTwo, changeStateThree};
    // form input state
    const [step, setStep] = useState(1);
    const [funderType, setFunderType] = useState("");
    const [fPhoneNo, setFPhoneNo] = useState("");
    const [fEmail, setFEmail] = useState("");
    const [fFirstName, setFFirstName] = useState("");
    const [fLastName, setFLastName] = useState("");
    const [interest, setInterest] = useState("");
    const [termsRoi, setTermsRoi] = useState("");
    const [fOrganisationName, setFOrganisationName] = useState("");
  
    const FormItem = {funderType, setFunderType, fPhoneNo, setFPhoneNo, fEmail, fFirstName, setFFirstName, setFEmail, interest, fOrganisationName, setFOrganisationName, setInterest, changeStateOne, fLastName, setFLastName, termsRoi, setTermsRoi, changeStateTwo, changeStateThree};
    
      // Go to next step
  const nextStep = () => {
    setStep(step + 1);
  };

  // Go to prev step
  const prevStep = () => {
    // const { step } = this.state;
    setStep(step - 1);
  };
//   back to first step 
    const firstStep = () => {
    setStep(1);
  };
  //   back to second step 
  const secondStep = () => {
    setStep(2);
  };
  switch (step) {

  case 1:
      return (
          <Basic 
              {...FormItem}
              nextStep={nextStep}
              // handleChange={handleChange}
          />
      );
  case 2:
      return (
          <Details
              {...FormItem}
              nextStep={nextStep}
              prevStep={prevStep}
          />

      );
  case 3:
      return (
          <Review
              {...FormItem}
              firstStep={firstStep}
              secondStep={secondStep}
          />
      );
      default:
          return null;
  }
}
export default FinancierControl;