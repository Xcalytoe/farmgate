import Link from 'next/link';

const Submit = ({ navigation }) => {
  // const { go } = navigation;
  return (
    <div className="form_step">
      <div>
        <header className="auth_header px-4 md:px-16 pt-4 md:pt-10 pb-6">
              <div className="text-center">
                  <Link href="/">
                      <a>
                      <img className="inline-block" src="/images/farmgate_logo.svg" alt="Farmgate logo"/>
                      </a>
                  </Link>
              </div>
          </header>
      </div>
      <div className="page_body grid content-center">
       <div>
        <div className="success_svg_con text-center">
            <svg className="inline-block w-auto" width="78" height="78" viewBox="0 0 78 78" fill="none" xmlns="http://www.w3.org/2000/svg">
              <circle cx="39" cy="39" r="39" fill="#629215"/>
              <path d="M25 39L38 49L56 29" stroke="white" strokeWidth="2"/>
            </svg>

          </div>
          <div className="px-2 text-center">
            <h3 className="text-xl md:text-2xl leading-6 md:leading-8">Registration as Financier was successfully sent</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ipsum magna, egestas et orci nec, dignissim ullamcorper lorem. Nam ac semper est, at faucibus massa. Phasellus eu elit id nibh accumsan tempus sit amet quis erat. Ut porttitor facilisis nulla et elementum.</p>
            <Link href="/">
              <a className="text-base md:text-xl">Back to Main Page</a>
            </Link>
          </div>
       </div>
      </div>
    </div>
  );
};

export default Submit;